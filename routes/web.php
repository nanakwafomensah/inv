<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('dashboardstock',['as'=>'dashboardstock','uses'=>'DashboardController@indexstock']);

Route::get('customercat',['as'=>'customercat','uses'=>'CustomerController@indexcat']);
Route::get('productcat',['as'=>'productcat','uses'=>'ProductController@indexcat']);
Route::get('profile',['as'=>'profile','uses'=>'ProfileController@index']);
Route::get('warehouse',['as'=>'warehouse','uses'=>'WarehouseController@index']);
Route::get('userutil',['as'=>'userutil','uses'=>'UserutilController@index']);
Route::get('customer',['as'=>'customer','uses'=>'CustomerController@indexcust']);
Route::get('supplier',['as'=>'supplier','uses'=>'SupplierController@index']);
Route::get('product',['as'=>'product','uses'=>'ProductController@indexpro']);
Route::get('billing',['as'=>'billing','uses'=>'BillingController@index']);

Route::get('billingselectbox',['as'=>'billingselectbox','uses'=>'BillingController@billingselectbox']);

Route::post('login',['as'=>'login','uses'=>'LoginController@postLogin']);
Route::post('logout',['as'=>'logout','uses'=>'LoginController@postlogout']);
Route::post('saveuser',['as'=>'saveuser','uses'=>'UserutilController@save']);

Route::post('savewarehouse',['as'=>'savewarehouse','uses'=>'WarehouseController@save']);
Route::post('updatewarehouse',['as'=>'updatewarehouse','uses'=>'WarehouseController@update']);
Route::post('deletewarehouse',['as'=>'deletewarehouse','uses'=>'WarehouseController@delete']);

Route::post('saveproductcat',['as'=>'saveproductcat','uses'=>'ProductController@save']);
Route::post('updateproductcat',['as'=>'updateproductcat','uses'=>'ProductController@update']);
Route::post('deleteproductcat',['as'=>'deleteproductcat','uses'=>'ProductController@delete']);

Route::post('savecustomercat',['as'=>'savecustomercat','uses'=>'CustomerController@save']);
Route::post('updatecustomercat',['as'=>'updatecustomercat','uses'=>'CustomerController@update']);
Route::post('deletecustomercat',['as'=>'deletecustomercat','uses'=>'CustomerController@delete']);

Route::post('savecustomer',['as'=>'savecustomer','uses'=>'CustomerController@savecust']);
Route::post('updatecustomer',['as'=>'updatecustomer','uses'=>'CustomerController@updatecust']);
Route::post('deletecustomer',['as'=>'deletecustomer','uses'=>'CustomerController@deletecust']);

Route::post('savesupplier',['as'=>'savesupplier','uses'=>'SupplierController@save']);
Route::post('updatesupplier',['as'=>'updatesupplier','uses'=>'SupplierController@update']);
Route::post('deletesupplier',['as'=>'deletesupplier','uses'=>'SupplierController@delete']);

Route::post('saveproduct',['as'=>'saveproduct','uses'=>'ProductController@savepro']);
Route::post('updateproduct',['as'=>'updateproduct','uses'=>'ProductController@updatepro']);
Route::post('deleteproduct',['as'=>'deleteproduct','uses'=>'ProductController@deletepro']);

Route::post('savebill',['as'=>'savebill','uses'=>'BillingController@save']);

Route::post('updateprofile',['as'=>'updateprofile','uses'=>'ProfileController@save']);