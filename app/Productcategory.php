<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productcategory extends Model
{
    //
    protected $fillable=['name'];
    
    public function product(){
        return $this->hasMany(Product::class);
    }
}
