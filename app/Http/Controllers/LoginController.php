<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
class LoginController extends Controller
{
    //
    public function postLogin(Request $request){
        try{
            $credentials = [
                'username'    => $request->username,
                'password' => $request->password,
            ];
            if(Sentinel::authenticate($credentials)){
                return redirect('dashboardstock');
            }else
            {
                return redirect()->back()->with(['error'=>'wrong credentials']);
            }
        }catch(ThrottlingException $e){
            $delay=$e->getDelay();
            return redirect()->back()->with(['error'=>"You are banned for $delay seconds."]);

        }catch (NotActivatedException $e){
            return redirect()->back()->with(['error'=>"Your account has not been activated"]);
        }

    }
    public function postlogout(){
        Sentinel::logout();
        return redirect('/');
    }
}
