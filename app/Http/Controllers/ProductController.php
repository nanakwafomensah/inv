<?php

namespace App\Http\Controllers;

use App\Product;
use App\Productcategory;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    public function indexcat(){
        $productcat = Productcategory::all();
        return view('productcat')->with(['productcat'=>$productcat]);
    }
    public function save(Request $request){
        Productcategory::create($request->all());
        return redirect('productcat');
    }
    public function update(Request $request){
        $productcat =Productcategory::find($request->idEdit);
        $productcat->name = $request->nameEdit;
        $productcat->save();
        return redirect('productcat');
    }
    public function delete(Request $request){
        Productcategory::find($request->idDelete)->delete();
        return redirect('productcat');
    }
    
    /***************Product***************/
    public function indexpro(){
        $product=Product::all();
        return view('product')->with(['product'=>$product]);
    }
    public function savepro(Request $request){
        Product::create($request->all());
        return redirect('product');
    } 
    public function updatepro(Request $request){
        $product =Product::find($request->idEdit);
        $product->productname = $request->productnameEdit;
        $product->brandname = $request->brandnameEdit;
        $product->productquantity = $request->productquantityEdit;
        $product->productrate = $request->productrateEdit;
        $product->productpayment = $request->productpaymentEdit;
        $product->warehouse_id = $request->warehouse_idEdit;
        $product->productcategory_id = $request->productcategory_idEdit;
        $product->supplier_id = $request->supplier_idEdit;
        $product->save();
        return redirect('product');
    }
    public function deletepro(Request $request){
        Product::find($request->idDelete)->delete();
        return redirect('product');
    }
}
