<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use App\Customercategory;
class CustomerController extends Controller
{
    //
    public function indexcat(){
        $customercat=Customercategory::all();
        return view('customercat')->with(['customercat'=>$customercat]);;
    }
    public function save(Request $request){
        Customercategory::create($request->all());
        return redirect('customercat');
    }
    public function update(Request $request){
        $customercat =Customercategory::find($request->idEdit);
        $customercat->name = $request->nameEdit;
        $customercat->save();
        return redirect('customercat');
    }
    public function delete(Request $request){
        Customercategory::find($request->idDelete)->delete();
        return redirect('customercat');
    }

    /***********************customer details**************/
    public function indexcust(){
       $customers =Customer::all();  $customercategory =Customercategory::all();
        return view('customer')->with(['customers'=>$customers,'customercategory'=>$customercategory]);
    }
    public function savecust(Request $request){
        Customer::create($request->all());
        return redirect('customer');
    }
    public function updatecust(Request $request){
        $customercat =Customer::find($request->idEdit);
        $customercat->name = $request->nameEdit;
        $customercat->phonenumber = $request->phonenumberEdit;
        $customercat->address = $request->addressEdit;
        $customercat->customercategory_id = $request->customercategory_idEdit;
        $customercat->save();
        return redirect('customer');
    }
    public function deletecust(request $request){
        Customer::find($request->idDelete)->delete();
        return redirect('customer');
    }
}
