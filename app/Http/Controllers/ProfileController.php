<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use DB;
use Image;
class ProfileController extends Controller
{
    //
    public function index(){
        $profile =  DB::table('profiles')->first();
        return view('profile')->with(['profile'=>$profile]);
    }

    public function  save(Request $request){
        if($request->hasFile('logo')){
            $avatar=$request->file('logo');
            $filename=time().'.'.$avatar->getClientOriginalExtension();


            $profile = Profile::findorfail($request->profileid);
            $profile->companyname=$request->companyname;
            $profile->phone=$request->phone;
            $profile->email=$request->email;
            $profile->address=$request->address;
            $profile->mobile=$request->mobile;
            $profile->website=$request->website;
            $profile->fax=$request->fax;
            $profile->logo=$filename;
            if($profile->update()){
                Image::make($avatar)->resize(300,300)->save( public_path('/uploads/avatars/'.$filename));
                return redirect('profile');
            }
        }else{
            $profile = Profile::findorfail($request->profileid);
            $profile->companyname=$request->companyname;
            $profile->phone=$request->phone;
            $profile->email=$request->email;
            $profile->address=$request->address;
            $profile->mobile=$request->mobile;
            $profile->website=$request->website;
            $profile->fax=$request->fax;
            if($profile->update()){
                return redirect('profile');
            }

        }
    }
}
