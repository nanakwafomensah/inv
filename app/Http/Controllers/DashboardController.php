<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    public function indexstock(){
        return view('dashboardstock');
    }
    public function indexcustomer(){
        return view('dashboardcustomer');
    }
}
