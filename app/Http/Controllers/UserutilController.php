<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Sentinel;

class UserutilController extends Controller
{
    //
    public function index(){
        return view('userutil');
    }
    public function save(Request $request){
        $user=Sentinel::RegisterAndActivate($request->all());
        $role=Sentinel::findRoleBySlug($request->role);
        $role->users()->attach($user);
        return redirect('userutil');
    }
}
