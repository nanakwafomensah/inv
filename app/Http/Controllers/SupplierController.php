<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    //
    public function index(){
        $suppliers =Supplier::all();
        return view('supplier')->with(['suppliers'=>$suppliers]);

    }
    public function save(Request $request){
        Supplier::create($request->all());
        return redirect('supplier');
    }
    public function update(Request $request){
        $supplier =Supplier::find($request->idEdit);
        $supplier->name = $request->nameEdit;
        $supplier->phonenumber = $request->phonenumberEdit;
        $supplier->address = $request->addressEdit;
        $supplier->save();
        return redirect('supplier');
    }
    public function delete(request $request){
        Supplier::find($request->idDelete)->delete();
        return redirect('supplier');
    }
}
