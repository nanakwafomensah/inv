<?php

namespace App\Http\Controllers;

use App\Warehouse;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
    //
    public function index(){
        $warehouse=Warehouse::all();
        return view('warehouse')->with(['warehouse'=>$warehouse]);
    }

    public function save(Request $request){
        Warehouse::create($request->all());
        return redirect('warehouse');
    }
    public function update(Request $request){
        $warehouse =Warehouse::find($request->idEdit);
        $warehouse->name = $request->nameEdit;
        $warehouse->location = $request->locationEdit;
        $warehouse->save();
        return redirect('warehouse');
    }
    public function delete(Request $request){
        Warehouse::find($request->idDelete)->delete();
        return redirect('warehouse');
    }
}
