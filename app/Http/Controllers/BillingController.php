<?php

namespace App\Http\Controllers;

use App\Billing;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use App\Product;
class BillingController extends Controller
{
    

    //
    public function index(){
        
        return view('billing');
    }
    public function billingselectbox(){
        $outputproductdropdown='';
        $product=Product::all();
        foreach ($product as $p){
            $outputproductdropdown.='<option value="'.$p->id.'">'.$p->productname.'</option>';
        }
        
        return Response($outputproductdropdown);
    }
    public function save(Request $request){
       $bill= new Billing();
        $number_of_items=$request->number_of_items;


        for($i=0;$i<=$number_of_items;$i++){
               $bill->customer_id= $request->customer;
               $bill->product_id=$request->productname[0];
               $bill->billingnumber=$request->billnumber;
               $bill->quantity=$request->quantity[0];
               $bill->amount=$request->amount[0];
               $bill->total=$request->total[0];
               $bill->save();

        }
    }
    public function update(){
        
    }
    public function delete(){
        
    }
    
}
