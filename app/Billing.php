<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    //
    protected $fillable=['billingnumber','customer_id','product_id','quantity','amount','total'];

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
