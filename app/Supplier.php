<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    //
    protected  $fillable=['name','phonenumber','address'];
    
    public function product(){
        return $this->hasMany(Product::class);
    }
}
