<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable =['productname','productcategory_id','brandname',
        'productquantity','productrate','supplier_id','warehouse_id',
        'productpayment'];
    
    public function warehouse(){
        return $this->belongsTo(Warehouse::class);
    }
    public function productcategory(){
        return $this->belongsTo(Productcategory::class);
    }
    public function supplier(){
        return $this->belongsTo(Supplier::class);
    }
    public function billing(){
        return $this->hasMany(Billing::class);
    }
    public function customer(){
        return $this->belongsTo(Customer::class);
    }
}
