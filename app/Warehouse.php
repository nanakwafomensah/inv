<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    //
  
    protected $fillable=['name','location'];
    public function product(){
        return $this->hasMany(Product::class);
    }
}
