<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable=['name','phonenumber','address','customercategory_id'];
    //
    public function Customercategory(){
        return $this->belongsTo(Customercategory::class);
    }
    public function product(){
        return $this->hasMany(Product::class);
    }
    public function billing(){
        return $this->hasMany(Billing::class);
    }
}
