<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>fast & easy</title>
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#27ae60">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/elephant.min.css">
    <link rel="stylesheet" href="assets/css/application.min.css">
    <link rel="stylesheet" href="assets/css/demo.min.css">
    <link rel="stylesheet" href="assets/parsley/css/parsley.css">
</head>
<body class="layout layout-header-fixed">
<div class="layout-header">
    @include('template.topbar')
</div>
<div class="layout-main">
    <div class="layout-sidebar" >
        <div class="layout-sidebar-backdrop" style="background-color: #217345;"></div>
        <div class="layout-sidebar-body" style="background-color: #217345;">
            <div class="custom-scrollbar" >
                <nav id="sidenav" class="sidenav-collapse collapse" style="background-color: #217345; color: #fff">
                    <ul class="sidenav">

                        <li class="sidenav-heading">Dashboards</li>
                        <li class="sidenav-item active">
                            <a href="dashboardstock" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-users"></span>
                                <span class="sidenav-label" style="font-size: 11px">Stock</span>
                            </a>
                        </li>
                        {{--<li class="sidenav-item  ">--}}
                            {{--<a href="dashboardcustomer" aria-haspopup="true">--}}
                                {{--<span class="sidenav-icon icon icon-male icon-building"></span>--}}
                                {{--<span class="sidenav-label" style="font-size: 11px">Customers</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="sidenav-item ">--}}
                            {{--<a href="rewards" aria-haspopup="true">--}}
                                {{--<span class="sidenav-icon icon icon-money"></span>--}}
                                {{--<span class="sidenav-label" style="font-size: 11px">Rewards</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}

                        <li class="sidenav-heading">System Configuration</li>
                        <li class="sidenav-item ">
                            <a href="warehouse" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-globe"></span>
                                <span class="sidenav-label" style="font-size: 11px">Warehouse</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="customercat" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-globe"></span>
                                <span class="sidenav-label" style="font-size: 11px">Customer Category</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="productcat" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-car"></span>
                                <span class="sidenav-label" style="font-size: 11px">Product Type</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="profile" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-building"></span>
                                <span class="sidenav-label" style="font-size: 11px">Profile</span>
                            </a>

                        </li>


                        <li class="sidenav-heading">Customer & Supplier</li>
                        <li class="sidenav-item ">
                            <a href="customer" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">Customer</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="supplier" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">Supplier</span>
                            </a>

                        </li>


                        <li class="sidenav-heading">Stock</li>
                        <li class="sidenav-item ">
                            <a href="product" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">Product</span>
                            </a>

                        </li>
                        <li class="sidenav-heading">Sales</li>
                        <li class="sidenav-item ">
                            <a href="billing" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-money"></span>
                                <span class="sidenav-label" style="font-size: 11px">Billing</span>
                            </a>
                        </li>
                        <li class="sidenav-heading">Report</li>
                        <li class="sidenav-item ">
                            <a href="rewardengine" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Daily Report</span>
                            </a>

                        </li>

                        <li class="sidenav-heading">User Management</li>

                        <li class="sidenav-item ">
                            <a href="#createUser" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px" data-toggle="modal" data-target="#createUser">Create New User</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="userutil" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-edit"></span>
                                <span class="sidenav-label" style="font-size: 11px" >User Utilities</span>
                            </a>

                        </li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">

                <h1 class="title-bar-title">
                    <span class="d-ib">Stocks</span>
                </h1>
                <p class="title-bar-description">
                    <small  style ="color:#217345">Insights &amp Analytics</small>

                </p>
            </div>

            <div class = "row">
                <div class = "col-lg-12">

                    <div class="col-md-2">
                        <div class="card" style="background-color: #217345; color: #fff">
                            <div class="card-values">
                                <div class="p-x">
                                    <small>Total Stocks <i class = "icon icon-users" style = "color: #fff"></i></small><br><br>
                                    <h3 class="card-title fw-l">5</h3>
                                </div>
                            </div>
                            <div class="card-chart">
                                <canvas data-chart="line" data-animation="false" data-labels='["Jun 21", "Jun 20", "Jun 19", "Jun 18", "Jun 17", "Jun 16", "Jun 15"]' data-values='[{"backgroundColor": "rgba(39, 174, 96, 0.03)", "borderColor": "#fff", "data": [25250, 23370, 25568, 28961, 26762, 30072, 25135]}]' data-scales='{"yAxes": [{ "ticks": {"max": 32327}}]}' data-hide='["legend", "points", "scalesX", "scalesY", "tooltips"]' height="35"></canvas>
                            </div>
                        </div>

                    </div></div></div>



            <div class="row">
                <div class = "col-lg-12">

                    <div class="col-md-2">
                        <div class="card" style="background-color: #217345; color: #fff">
                            <div class="card-values">
                                <div class="p-x">
                                    <small>Active Stocks <i class = "icon icon-credit-card" style = "color: #fff"></i> <span> </span></small><br><br>
                                    <h3 class="card-title fw-l">4</h3>
                                </div>
                            </div>
                            <div class="card-chart">
                                <canvas data-chart="line" data-animation="false" data-labels='["Jun 21", "Jun 20", "Jun 19", "Jun 18", "Jun 17", "Jun 16", "Jun 15"]' data-values='[{"backgroundColor": "rgba(39, 174, 96, 0.03)", "borderColor": "#fff", "data": [116196, 145160, 124419, 147004, 134740, 120846, 137225]}]' data-scales='{"yAxes": [{ "ticks": {"max": 158029}}]}' data-hide='["legend", "points", "scalesX", "scalesY", "tooltips"]' height="35"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="card" style="background-color: #217345; color: #fff">
                            <div class="card-values">
                                <div class="p-x">
                                    <small>Inactive Stocks <i class = "" style = "color: #fff"></i><span> </span></small><br><br>
                                    <h3 class="card-title fw-l">4</h3>
                                </div>
                            </div>
                            <div class="card-chart">
                                <canvas data-chart="line" data-animation="false" data-labels='["Jun 21", "Jun 20", "Jun 19", "Jun 18", "Jun 17", "Jun 16", "Jun 15"]' data-values='[{"backgroundColor": "rgba(39, 174, 96, 0.03)", "borderColor": "#fff", "data": [13590442, 12362934, 13639564, 13055677, 12915203, 11009940, 11542408]}]' data-scales='{"yAxes": [{ "ticks": {"max": 14662531}}]}' data-hide='["legend", "points", "scalesX", "scalesY", "tooltips"]' height="35"></canvas>
                            </div>
                        </div>
                    </div>
                </div></div>
            <br><br>
            <div class="row">

                <div class="col-md-8">
                    <div class="card" >
                        <div class="card-body" >
                            <h6 class="card-title">Network - Subscriber Statistical Distribution</h6>
                        </div>
                        <div class="card-body">
                            <div class="card-chart">
                                <canvas data-chart="bar" data-animation="false" data-labels='["MTN", "Vodafone", "Tigo", "Airtel", "Glo"]' data-values='[{"label": "Active", "backgroundColor": "#217345", "borderColor": "#217345", "data": ["mtn", "voda", "tigo", "airtel", "glo"]}, {"label": "Disabled", "backgroundColor": "gainsboro", "borderColor": "gainsboro", "data": [3, 3,3, 3, 3]}]' data-tooltips='{"mode": "label"}'  height="80"></canvas>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="panel panel-body" data-toggle="match-height">
                        <h6 class="text-left m-t-0">Rewards Distribution</h6>
                        <div class="row">

                            <div class="col-xs-3">
                                <br><br>
                                <ul class="list-unstyled">
                                    <li class="m-b">
                                        <small class="nowrap">
                                            <span class="icon icon-square icon-fw" style="color: #217345"></span>
                                            Mobile Money
                                        </small>
                                    </li>
                                    <li class="m-b">
                                        <small class="nowrap">
                                            <span class="icon icon-square icon-fw" style="color: gainsboro"></span>
                                            Airtime
                                        </small>
                                    </li>


                                </ul>
                            </div>
                            <div class="col-xs-6">
                                <canvas data-chart="pie" data-labels='["Mobile Money", "Airtime"]' data-values='[{"backgroundColor": ["#217345", "gainsboro"], "data": [3, 3]}]' data-hide='["scalesX", "scalesY", "legend"]' height="395" width="300"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




        </div></div>



    @include('template.logoutView')

    @include('template.createuserView')

    @include('template.changepasswordview')

    <div id="cardIssuance" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header bg-primary" style="background-color: #217345">
                    <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                        <span aria-hidden="true" style="color: #fff">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <div class="text-center">
                        <span class="icon icon-credit-card icon-5x m-y-lg"></span>
                        <h4 class="modal-title" style="font-size: 12px">Card Issuance</h4>

                    </div>
                </div>
                <div class="modal-tabs">

                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="display11">
                            <form id="myform">
                                <div class ="row">
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <label  class="form-label" style="font-size: 12px">Subscriber's Phone Number</label>
                                            <input id="form-control-111" class="form-control" type="text" style="font-size: 11px" required>
                                        </div>

                                        <div class="col-md-2">
                                            <label  class="form-label" style="font-size: 12px"></label>
                                            <center> <button type="submit" class="check btn btn-primary" id="checko" style="background-color: goldenrod;margin-top: 3px"><i class="icon icon-check"></i> Check</button></center>
                                        </div>

                                    </div></div><hr>
                            </form>



                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <label  class="form-label" style="font-size: 12px">Subsriber's Name</label>
                                        <input id="form-control-21" class="form-control" type="text" style="font-size: 11px" readonly>

                                    </div>


                                    <div class="col-md-2">
                                        <label  class="form-label" style="font-size: 12px">Card Status</label>
                                        <center id="show_icon">   </center>

                                    </div></div></div>
                            <hr><br><br>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label  class="form-label" style="font-size: 12px">Click In The Text Area (Initiate Card#)</label>
                                        <input id="form-control-211" class="form-control" type="text" style="font-size: 11px" required>

                                    </div>


                                </div></div>


                        </div>

                    </div>
                </div>
                <div class="modal-footer">

                    <center>   <button type="submit" id="write_card" class="btn btn-primary" style="background-color: #217345;margin-top: 3px">&nbsp <i class="icon icon-thumbs-up"></i>  Write To Card</button></center>
                </div>
            </div>

        </div>
    </div>


    <div id="createUserP" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="UserRoles/storeuserparent" method="post" data-parsley-validate="">
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-user-plus icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 12px">Create A New Parent User</h4>

                        </div>
                    </div>
                    <div class="modal-tabs">

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="display2">

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <label  class="form-label" style="font-size: 12px">Company name</label>
                                            <select id="demo-select2-2" class="form-control" style="font-size: 11px" name="companyname" required>
                                                <option value="">Choose an OMC</option>
                                                <option value="frimps oil">FRIMPS OIL</option>
                                                <option value="goil">GOIL</option>


                                            </select>
                                            <div class="col-md-4">
                                                <!--                                            <label  class="form-label" style="font-size: 12px">Sex</label>-->
                                                <!---->
                                                <!--                                            <select id="demo-select2-2" class="form-control" style="font-size: 11px" name="sex">-->
                                                <!--                                                <option value="M" >Male</option>-->
                                                <!--                                                <option value="F">Female</option>-->
                                                <!--                                                <option value="O">Other</option>-->
                                                <!---->
                                                <!--                                            </select>-->
                                            </div>

                                        </div></div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <label  class="form-label" style="font-size: 12px">User Email</label>
                                                <input id="form-control-7" class="form-control" type="email" name="email" style="font-size: 11px" required="">
                                            </div>


                                            <div class="col-md-4">
                                                <label  class="form-label" style="font-size: 12px">Phone Number</label>
                                                <input id="form-control-10" class="form-control" type="text" name="phonenumber" style="font-size: 11px" required="">
                                            </div>

                                        </div>
                                    </div><hr style="border-color: #217345">

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label  class="form-label" style="font-size: 12px">User Role</label>
                                                <select id="demo-select2-3" class="form-control" name="userrole" style="font-size: 11px" disabled>

                                                    <option value="1" >admin</option>


                                                </select>
                                            </div>



                                        </div>
                                    </div>




                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">

                            <button type="submit" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                        </div>
                    </div>
            </form>
        </div>
    </div>



    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/elephant.min.js"></script>
    <script src="assets/js/application.min.js"></script>
    <script src="assets/js/demo.min.js"></script>
    <script src="assets/js/toastr.min.js"></script>
    <script src="assets/parsley/js/parsley.min.js"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-83990101-1', 'auto');
        ga('send', 'pageview');
    </script>



</body>
</html>