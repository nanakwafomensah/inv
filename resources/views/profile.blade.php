<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>fast & easy</title>
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#27ae60">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/elephant.min.css">
    <link rel="stylesheet" href="assets/css/application.min.css">
    <link rel="stylesheet" href="assets/css/demo.min.css">
    <link rel="stylesheet" href="assets/parsley/css/parsley.css">

</head>
<body class="layout layout-header-fixed">
<div class="layout-header">
    @include('template.topbar')
</div>
<div class="layout-main">
    <div class="layout-sidebar" >
        <div class="layout-sidebar-backdrop" style="background-color: #217345;"></div>
        <div class="layout-sidebar-body" style="background-color: #217345;">
            <div class="custom-scrollbar" >
                <nav id="sidenav" class="sidenav-collapse collapse" style="background-color: #217345; color: #fff">
                    <ul class="sidenav">

                        <li class="sidenav-heading">Dashboards</li>
                        <li class="sidenav-item ">
                            <a href="dashboardstock" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-users"></span>
                                <span class="sidenav-label" style="font-size: 11px">Stock</span>
                            </a>
                        </li>
                        {{--<li class="sidenav-item ">--}}
                            {{--<a href="dashboardcustomer" aria-haspopup="true">--}}
                                {{--<span class="sidenav-icon icon icon-male icon-building"></span>--}}
                                {{--<span class="sidenav-label" style="font-size: 11px">Customers</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="sidenav-item ">--}}
                        {{--<a href="rewards" aria-haspopup="true">--}}
                        {{--<span class="sidenav-icon icon icon-money"></span>--}}
                        {{--<span class="sidenav-label" style="font-size: 11px">Rewards</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}

                        <li class="sidenav-heading">System Configuration</li>
                        <li class="sidenav-item ">
                            <a href="warehouse" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-globe"></span>
                                <span class="sidenav-label" style="font-size: 11px">Warehouse</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="customercat" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-globe"></span>
                                <span class="sidenav-label" style="font-size: 11px">Customer Category</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="productcat" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-car"></span>
                                <span class="sidenav-label" style="font-size: 11px">Product Type</span>
                            </a>

                        </li>
                        <li class="sidenav-item active">
                            <a href="profile" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-building"></span>
                                <span class="sidenav-label" style="font-size: 11px">Profile</span>
                            </a>

                        </li>


                        <li class="sidenav-heading">Customer & Supplier</li>
                        <li class="sidenav-item ">
                            <a href="customer" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">Customer</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="supplier" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">Supplier</span>
                            </a>

                        </li>


                        <li class="sidenav-heading">Stock</li>
                        <li class="sidenav-item ">
                            <a href="product" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">Add A New Product</span>
                            </a>

                        </li>
                        <li class="sidenav-heading">Sales</li>
                        <li class="sidenav-item ">
                            <a href="rewardengine" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-money"></span>
                                <span class="sidenav-label" style="font-size: 11px">Billing</span>
                            </a>
                        </li>
                        <li class="sidenav-heading">Report</li>
                        <li class="sidenav-item ">
                            <a href="rewardengine" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Daily Report</span>
                            </a>

                        </li>

                        <li class="sidenav-heading">User Management</li>

                        <li class="sidenav-item ">
                            <a href="#createUser" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px" data-toggle="modal" data-target="#createUser">Create New User</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="userutil" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-edit"></span>
                                <span class="sidenav-label" style="font-size: 11px" >User Utilities</span>
                            </a>

                        </li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="layout-content">
        <div class="layout-content-body">



            <br><br>
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-actions">

                                <button type="button" class="card-action card-reload" title="Reload"></button>

                            </div>

                        </div>
                        <div class="card-body">

                            <form class="form" action="updateprofile" method="post" id="registrationForm" enctype="multipart/form-data">
                                @if(!empty($profile->id))
                                    <input name="profileid" value="{{$profile->id}}" type="hidden"/>
                                @endif
                                    <div class="col-md-4">

                                    </div>
                                    <div class="col-md-4">

                                    </div>
                                    <div class="col-md-4">
                                        @if(!empty($profile->logo))
                                            <div><img id="preview_image" src="uploads/avatars/{{$profile->logo}}" width="270px" height="270px" class="img-responsive text-center" ></div>
                                        @else
                                            <div><img src="uploads/avatars/download.png" width="270px" height="270px" class="img-responsive text-center" ></div>
                                        @endif
                                        <input type="file" id="imgInp" name="logo" class="form-control">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    </div>


                                <div class="form-group">

                                    <div class="col-xs-6">
                                        <label for="first_name"><small>Company Name</small></label>
                                        <input type="text" class="form-control" name="companyname" value="{{$profile->companyname}}" id="first_name" placeholder="" title="enter your first name if any.">
                                    </div>
                                </div>
                                <div class="form-group">

                                    <div class="col-xs-6">
                                        <label for="last_name"><small>Phycal Address</small></label>
                                        <input type="text" class="form-control" name="address" value="{{$profile->address}}" id="last_nae" placeholder="" title="enter your last name if any.">
                                    </div>
                                </div>

                                <div class="form-group">

                                    <div class="col-xs-6">
                                        <label for="phone"><small>Phone</small></label>
                                        <input type="text" class="form-control" name="phone" value="{{$profile->phone}}"id="phone" placeholder="" title="enter your phone number if any.">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-6">
                                        <label for="mobile"><small>Mobile</small></label>
                                        <input type="text" class="form-control" name="mobile" value="{{$profile->mobile}}" id="mobile" placeholder="" title="enter your mobile number if any.">
                                    </div>
                                </div>
                                <div class="form-group">

                                    <div class="col-xs-6">
                                        <label for="email"><small>Email</small></label>
                                        <input type="email" class="form-control" name="email" value="{{$profile->email}}" id="email" placeholder="" title="enter your email.">
                                    </div>
                                </div>
                                <div class="form-group">

                                    <div class="col-xs-6">
                                        <label for="email"><small>Website</small></label>
                                        <input type="text" class="form-control" name="website" value="{{$profile->website}}" id="location" placeholder="" title="enter a location">
                                    </div>
                                </div>
                                <div class="form-group">

                                    <div class="col-xs-6">
                                        <label for="password"><small>Fax</small></label>
                                        <input type="text" class="form-control" name="fax" value="{{$profile->fax}}"id="password" placeholder="" title="enter your password.">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <br>
                                        <div class="pull-left">
                                            <button class=" btn  btn-success" style = "text-transform: capitalize; background-color: #217345" data-toggle="modal" data-target="#newDistrict" type="submit"> <i class = "icon icon-save"></i> &nbsp Save</button>
                                        </div>
                                    </div>
                                </div>
                            </form>




                           </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    @include('template.logoutView')

    @include('template.createuserView')

    @include('template.changepasswordview')




    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/elephant.min.js"></script>
    <script src="assets/js/application.min.js"></script>
    <script src="assets/js/demo.min.js"></script>
    <script src="assets/parsley/js/parsley.min.js"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-83990101-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="assets/js/toastr.min.js"></script>






</body>
</html>