<div class="navbar navbar-default" style = "background-color: #fff">
    <div class="navbar-header" style = "background-color: #217345">
        <a class="navbar-brand navbar-brand-center" href="audience.html">
            fast & easy
            {{--<img class="navbar-brand-logo" src="assets/im" alt=""  style = " height: 44px; width: 212px;margin-top: -10px ">--}}
        </a>
        <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
            <span class="sr-only">Toggle navigation</span>
            <span class="bars" >
              <span class="bar-line bar-line-1 out"></span>
              <span class="bar-line bar-line-2 out"></span>
              <span class="bar-line bar-line-3 out"></span>
            </span>
            <span class="bars bars-x">
              <span class="bar-line bar-line-4"></span>
              <span class="bar-line bar-line-5"></span>
            </span>
        </button>
        <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="arrow-up"></span>
            <span class="ellipsis ellipsis-vertical">
              <img class="ellipsis-object" width="32" height="32" src="img/0180441436.jpg" alt="Teddy Wilson">
            </span>
        </button>
    </div>
    <div class="navbar-toggleable">
        <nav id="navbar" class="navbar-collapse collapse">
            <button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true" type="button">
                <span class="sr-only">Toggle navigation</span>
              <span class="bars">
                <span class="bar-line bar-line-1 out"></span>
                <span class="bar-line bar-line-2 out"></span>
                <span class="bar-line bar-line-3 out"></span>
                <span class="bar-line bar-line-4 in"></span>
                <span class="bar-line bar-line-5 in"></span>
                <span class="bar-line bar-line-6 in"></span></span>

            </button>
            <ul class="nav navbar-nav navbar-right">
                <li class="visible-xs-block">

                </li>


                <li class="dropdown hidden-xs">
                    <button class="navbar-account-btn"  aria-haspopup="true" style="color: #105d07">
                        <img class="rounded" width="30" height="30" src="assets/img/no.png" alt="" style="color: limegreen;">  <span style="font-family: Candara; font-size: 11px">
                                @if(Sentinel::check())
                                {{Sentinel::getUser()->fullname}}
                            @endif

                            &nbsp &nbsp </span>   <button class="btn  btn-success" style = "text-transform: capitalize; margin-top: 5px; background-color: #217345"  type="button" data-toggle="modal" data-target="#logout"> <i class = "icon icon-lock"></i> &nbsp Logout</button> &nbsp &nbsp <button class="btn  btn-success" style = "text-transform: capitalize; margin-top: 5px; background-color: #dbdbdb; color: #217345; border-color: #dbdbdb"  type="button" data-toggle="modal" data-target="#pwdChange"> <i class = "icon icon-edit"></i> &nbsp Change Password</button>

                    </button>


                </li>


                <li class="visible-xs-block">
                    <a href="login-1.html">
                        <span class="icon icon-power-off icon-lg icon-fw"></span>
                        Sign out
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>