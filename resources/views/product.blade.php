<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>fast & easy</title>
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#27ae60">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/elephant.min.css">
    <link rel="stylesheet" href="assets/css/application.min.css">
    <link rel="stylesheet" href="assets/css/demo.min.css">
    <link rel="stylesheet" href="assets/parsley/css/parsley.css">

</head>
<body class="layout layout-header-fixed">
<div class="layout-header">
    @include('template.topbar')
</div>
<div class="layout-main">
    <div class="layout-sidebar" >
        <div class="layout-sidebar-backdrop" style="background-color: #217345;"></div>
        <div class="layout-sidebar-body" style="background-color: #217345;">
            <div class="custom-scrollbar" >
                <nav id="sidenav" class="sidenav-collapse collapse" style="background-color: #217345; color: #fff">
                    <ul class="sidenav">

                        <li class="sidenav-heading">Dashboards</li>
                        <li class="sidenav-item ">
                            <a href="dashboardstock" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-users"></span>
                                <span class="sidenav-label" style="font-size: 11px">Stock</span>
                            </a>
                        </li>
                        {{--<li class="sidenav-item ">--}}
                        {{--<a href="dashboardcustomer" aria-haspopup="true">--}}
                        {{--<span class="sidenav-icon icon icon-male icon-building"></span>--}}
                        {{--<span class="sidenav-label" style="font-size: 11px">Customers</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="sidenav-item ">--}}
                        {{--<a href="rewards" aria-haspopup="true">--}}
                        {{--<span class="sidenav-icon icon icon-money"></span>--}}
                        {{--<span class="sidenav-label" style="font-size: 11px">Rewards</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}

                        <li class="sidenav-heading">System Configuration</li>
                        <li class="sidenav-item ">
                            <a href="warehouse" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-globe"></span>
                                <span class="sidenav-label" style="font-size: 11px">Warehouse</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="customercat" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-globe"></span>
                                <span class="sidenav-label" style="font-size: 11px">Customer Category</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="productcat" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-car"></span>
                                <span class="sidenav-label" style="font-size: 11px">Product Type</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="profile" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-building"></span>
                                <span class="sidenav-label" style="font-size: 11px">Profile</span>
                            </a>

                        </li>


                        <li class="sidenav-heading">Customer & Supplier</li>
                        <li class="sidenav-item ">
                            <a href="customer" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">Customer</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="supplier" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">Supplier</span>
                            </a>

                        </li>


                        <li class="sidenav-heading">Stock</li>
                        <li class="sidenav-item active">
                            <a href="product" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">Product</span>
                            </a>

                        </li>
                        <li class="sidenav-heading">Sales</li>
                        <li class="sidenav-item ">
                            <a href="billing" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-money"></span>
                                <span class="sidenav-label" style="font-size: 11px">Billing</span>
                            </a>
                        </li>
                        <li class="sidenav-heading">Report</li>
                        <li class="sidenav-item ">
                            <a href="dailyreport" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Daily Report</span>
                            </a>

                        </li>

                        <li class="sidenav-heading">User Management</li>

                        <li class="sidenav-item ">
                            <a href="#createUser" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px" data-toggle="modal" data-target="#createUser">Create New User</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="userutil" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-edit"></span>
                                <span class="sidenav-label" style="font-size: 11px" >User Utilities</span>
                            </a>

                        </li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">

                <h1 class="title-bar-title">
                    <span class="d-ib">Product</span>

                </h1>
                <p class="title-bar-description">
                    <small  style ="color:#217345">Utilities</small>

                </p>
            </div>


            <div class="pull-left">
                <button class=" btn  btn-success" style = "text-transform: capitalize; background-color: #217345" data-toggle="modal" data-target="#newDistrict" type="button"> <i class = "icon icon-plus-circle"></i> &nbsp New Product</button>


            </div>
            <br><br>
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-actions">

                                <button type="button" class="card-action card-reload" title="Reload"></button>

                            </div>

                        </div>
                        <div class="card-body">

                            <table id="demo-datatables-responsive-1" class="table table-bordered table-striped table-nowrap dataTable" cellspacing="0" width="100%" style="font-size: 12px">
                                <thead>
                                <tr>
                                    <th> Number</th>
                                    <th> Product Name</th>
                                    <th> Brand Name</th>
                                    <th> Quantity</th>
                                    <th> Rate</th>
                                    <th> Payment</th>
                                    <th> Warehouse</th>
                                    <th> Type</th>
                                    <th> Supplier</th>
                                    <th><center>Action</center></th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $x=1; ?>
                                @foreach($product as $p)

                                    <tr>
                                        <td>
                                            {{$x++}}
                                        </td>
                                        <td>
                                            {{$p->productname}}
                                        </td>
                                        <td>
                                            {{$p->brandname}}
                                        </td>
                                        <td>
                                            {{$p->productquantity}}
                                        </td>
                                        <td>
                                            {{$p->productrate}}
                                        </td>
                                        <td>
                                            {{$p->productpayment}}
                                        </td>
                                        <td>
                                            {{App\Product::find($p->id)->warehouse->name}}
                                        </td>
                                        <td>
                                            {{App\Product::find($p->id)->productcategory->name}}
                                        </td>
                                        <td>
                                            {{App\Product::find($p->id)->supplier->name}}
                                        </td>

                                        <td>
                                            <center>
                                                <button class="deletebtn btn  btn-warning" style = "text-transform: capitalize;  background-color: maroon; ; border-color: maroon"  type="button" data-toggle="modal" data-target="#delDistrict" data-id="{{$p->id}}" data-productname="{{$p->productname}}"> <i class = "icon icon-trash"></i></button>
                                                <button class="editbtn btn  btn-warning" style = "text-transform: capitalize;  background-color: goldenrod; ; border-color: goldenrod"  type="button" data-toggle="modal" data-target="#editDistrict"
                                                        data-id="{{$p->id}}"
                                                        data-productname="{{$p->productname}}"
                                                        data-brandname="{{$p->brandname}}"
                                                        data-productquantity="{{$p->productquantity}}"
                                                        data-productrate="{{$p->productrate}}"
                                                        data-productpayment="{{$p->productpayment}}"
                                                        data-warehouse_id="{{$p->warehouse_id}}"
                                                        data-productcategory_id="{{$p->productcategory_id}}"
                                                        data-supplier_id="{{$p->supplier_id}}"
                                                > <i class = "icon icon-edit"></i></button>
                                            </center>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    @include('template.logoutView')

    @include('template.createuserView')

    @include('template.changepasswordview')

    <div id="delDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="deleteproduct" method="POST">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff">
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-trash icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 14px">District</h4>

                        </div>
                    </div>
                    <div class="modal-tabs">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Delete <span id="productnameDelete"></span> From System?</a></li>

                        </ul>
                        <input type="hidden" id="idDelete" name="idDelete"/>

                    </div>
                    <div class="modal-footer">
                        <center><button type="submit" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-check"></i>&nbsp &nbsp Yes &nbsp &nbsp</button>
                            <button type="button" class="btn btn-danger" style="background-color: maroon"><i class="icon icon-close"></i> Cancel</button></center>

                    </div>
                </div>
            </form>
        </div>
    </div>


    <div id="editDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="updateproduct" method="post" data-parsley-validate="" id="editform">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-edit icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 12px">Edit Product Details</h4>

                        </div>
                    </div>
                    <div class="modal-tabs">

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="display3">
                                <div class="form-group">
                                    <div class="row">
                                        <input type="hidden" id="idEdit" name="idEdit"/>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Product Type</label>
                                            <select id="productcategory_idEdit" class="form-control" style="font-size: 11px" name="productcategory_idEdit">
                                                <option value="" >Select Type</option>
                                                @foreach(App\Productcategory::all() as $w)
                                                    <option value="{{$w->id}}">{{$w->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">Supplier</label>

                                            <select id="supplier_idEdit" class="form-control" style="font-size: 11px" name="supplier_idEdit">
                                                <option value="" >Select Supplier</option>
                                                @foreach(App\Supplier::all() as $w)
                                                    <option value="{{$w->id}}">{{$w->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>


                                </div>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Product Name</label>
                                            <input id="productnameEdit" name="productnameEdit"  class="form-control" type="text" style="font-size: 11px" required="">
                                        </div>

                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">Warehouse</label>

                                            <select id="warehouse_idEdit" class="form-control" style="font-size: 11px" name="warehouse_idEdit">
                                                <option value="" >Select Gender</option>
                                                @foreach(App\Warehouse::all() as $w)
                                                    <option value="{{$w->id}}">{{$w->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>


                                </div>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">Brand Name</label>
                                            <input id="brandnameEdit" name="brandnameEdit"  class="form-control" type="text" style="font-size: 11px" required="">
                                        </div>
                                        <div class="col-md-2">
                                            <label  class="form-label" style="font-size: 12px">Rate</label>
                                            <input id="productrateEdit" name="productrateEdit"  class="form-control" type="number" style="font-size: 11px" required="">

                                        </div>
                                        <div class="col-md-2">
                                            <label  class="form-label" style="font-size: 12px">Product quantity</label>
                                            <input id="productquantityEdit" name="productquantityEdit"  class="form-control" type="number" style="font-size: 11px" required="">

                                        </div>
                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">Product Payment</label>
                                            <input id="productpaymentEdit" name="productpaymentEdit"  class="form-control" type="number" style="font-size: 11px" required="">

                                        </div>


                                    </div>


                                </div>




                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="submit" id="savee" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="newDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="saveproduct" method="POST" data-parsley-validate="" id="storeform">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-user-plus icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 12px">New Product</h4>

                        </div>
                    </div>

                    <div class="modal-tabs">

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="display2">

                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Product Type</label>
                                            <select id="productcategory_id" class="form-control" style="font-size: 11px" name="productcategory_id">
                                                <option value="" >Select Type</option>
                                                @foreach(App\Productcategory::all() as $w)
                                                    <option value="{{$w->id}}">{{$w->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">Supplier</label>

                                            <select id="supplier_id" class="form-control" style="font-size: 11px" name="supplier_id">
                                                <option value="" >Select Supplier</option>
                                                @foreach(App\Supplier::all() as $w)
                                                    <option value="{{$w->id}}">{{$w->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>


                                </div>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Product Name</label>
                                            <input id="productname" name="productname"  class="form-control" type="text" style="font-size: 11px" required="">
                                        </div>

                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">Warehouse</label>

                                            <select id="warehouse_id" class="form-control" style="font-size: 11px" name="warehouse_id">
                                                <option value="" >Select Warehouse</option>
                                                @foreach(App\Warehouse::all() as $w)
                                                    <option value="{{$w->id}}">{{$w->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>


                                </div>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">Brand Name</label>
                                            <input id="brandname" name="brandname"  class="form-control" type="text" style="font-size: 11px" required="">
                                        </div>
                                        <div class="col-md-2">
                                            <label  class="form-label" style="font-size: 12px">Rate</label>
                                            <input id="productrate" name="productrate"  class="form-control" type="number" style="font-size: 11px" required="">

                                        </div>
                                        <div class="col-md-2">
                                            <label  class="form-label" style="font-size: 12px">Product quantity</label>
                                            <input id="productquantity" name="productquantity"  class="form-control" type="number" style="font-size: 11px" required="">

                                        </div>
                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">Product Payment</label>
                                            <input id="productpayment" name="productpayment"  class="form-control" type="number" style="font-size: 11px" required="">

                                        </div>


                                    </div>


                                </div>


                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" value="Submit"  id="save_data" class="btn btn-primary" style="background-color: #217345">
                    </div>


                </div>
            </form>
        </div>
    </div>



    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/elephant.min.js"></script>
    <script src="assets/js/application.min.js"></script>
    <script src="assets/js/demo.min.js"></script>
    <script src="assets/parsley/js/parsley.min.js"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-83990101-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="assets/js/toastr.min.js"></script>



    <script>
        $(document).on('click','.editbtn',function(){
            $('#productnameEdit').val($(this).data('productname'));
            $('#brandnameEdit').val($(this).data('brandname'));
            $('#productquantityEdit').val($(this).data('productquantity'));
            $('#productrateEdit').val($(this).data('productrate'));
            $('#productpaymentEdit').val($(this).data('productpayment'));
            $('#warehouse_idEdit').val($(this).data('warehouse_id')).select;
            $('#productcategory_idEdit').val($(this).data('productcategory_id')).select;
            $('#supplier_idEdit').val($(this).data('supplier_id')).select;
            $('#idEdit').val($(this).data('id'));


        });
    </script>
    <script>
        $(document).on('click','.deletebtn',function() {
            $('#idDelete').val($(this).data('id'));
            $("#productnameDelete").html($(this).data('productname'));

        });
    </script>


</body>
</html>