<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>fast & easy</title>
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#27ae60">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/elephant.min.css">
    <link rel="stylesheet" href="assets/css/application.min.css">
    <link rel="stylesheet" href="assets/css/demo.min.css">
    <link rel="stylesheet" href="assets/parsley/css/parsley.css">

</head>
<body class="layout layout-header-fixed">
<div class="layout-header">
    @include('template.topbar')
</div>
<div class="layout-main">
    <div class="layout-sidebar" >
        <div class="layout-sidebar-backdrop" style="background-color: #217345;"></div>
        <div class="layout-sidebar-body" style="background-color: #217345;">
            <div class="custom-scrollbar" >
                <nav id="sidenav" class="sidenav-collapse collapse" style="background-color: #217345; color: #fff">
                    <ul class="sidenav">

                        <li class="sidenav-heading">Dashboards</li>
                        <li class="sidenav-item ">
                            <a href="dashboardstock" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-users"></span>
                                <span class="sidenav-label" style="font-size: 11px">Stock</span>
                            </a>
                        </li>
                        {{--<li class="sidenav-item ">--}}
                        {{--<a href="dashboardcustomer" aria-haspopup="true">--}}
                        {{--<span class="sidenav-icon icon icon-male icon-building"></span>--}}
                        {{--<span class="sidenav-label" style="font-size: 11px">Customers</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="sidenav-item ">--}}
                        {{--<a href="rewards" aria-haspopup="true">--}}
                        {{--<span class="sidenav-icon icon icon-money"></span>--}}
                        {{--<span class="sidenav-label" style="font-size: 11px">Rewards</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}

                        <li class="sidenav-heading">System Configuration</li>
                        <li class="sidenav-item ">
                            <a href="warehouse" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-globe"></span>
                                <span class="sidenav-label" style="font-size: 11px">Warehouse</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="customercat" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-globe"></span>
                                <span class="sidenav-label" style="font-size: 11px">Customer Category</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="productcat" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-car"></span>
                                <span class="sidenav-label" style="font-size: 11px">Product Type</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="profile" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-building"></span>
                                <span class="sidenav-label" style="font-size: 11px">Profile</span>
                            </a>

                        </li>


                        <li class="sidenav-heading">Customer & Supplier</li>
                        <li class="sidenav-item ">
                            <a href="customer" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px">Customer</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="supplier" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">Supplier</span>
                            </a>

                        </li>


                        <li class="sidenav-heading">Stock</li>
                        <li class="sidenav-item ">
                            <a href="product" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">Product</span>
                            </a>

                        </li>
                        <li class="sidenav-heading">Sales</li>
                        <li class="sidenav-item active">
                            <a href="billing" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-money"></span>
                                <span class="sidenav-label" style="font-size: 11px">Billing</span>
                            </a>


                        </li>
                        <li class="sidenav-heading">Report</li>
                        <li class="sidenav-item ">
                            <a href="rewardengine" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Daily Report</span>
                            </a>

                        </li>

                        <li class="sidenav-heading">User Management</li>

                        <li class="sidenav-item ">
                            <a href="#createUser" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px" data-toggle="modal" data-target="#createUser">Create New User</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="userutil" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-edit"></span>
                                <span class="sidenav-label" style="font-size: 11px" >User Utilities</span>
                            </a>

                        </li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">

                <h1 class="title-bar-title">
                    <span class="d-ib">Billing</span>

                </h1>
                <p class="title-bar-description">
                    <small  style ="color:#217345">Utilities</small>

                </p>
            </div>


            <div class="pull-left">
                <button class=" btn  btn-success" style = "text-transform: capitalize; background-color: #217345" data-toggle="modal" data-target="#newDistrict" type="button"> <i class = "icon icon-plus-circle"></i> &nbsp New Bill</button>


            </div>
            <br><br>
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-actions">

                                <button type="button" class="card-action card-reload" title="Reload"></button>

                            </div>

                        </div>
                        <div class="card-body">

                            <table id="demo-datatables-responsive-1" class="table table-bordered table-striped table-nowrap dataTable" cellspacing="0" width="100%" style="font-size: 12px">
                                <thead>
                                <tr>
                                    <th> Number</th>
                                    <th> Category</th>
                                    <th> Name</th>
                                    <th> Phonenumber</th>
                                    <th> Address</th>
                                    <th><center>Action</center></th>

                                </tr>
                                </thead>
                                <tbody>


                                    <tr>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                        <td>

                                        </td>

                                        <td>
                                            <center>
                                                <button class="deletebtn btn  btn-warning" style = "text-transform: capitalize;  background-color: maroon; ; border-color: maroon"  type="button" data-toggle="modal" data-target="#delDistrict" > <i class = "icon icon-trash"></i></button>
                                                <button class="editbtn btn  btn-warning" style = "text-transform: capitalize;  background-color: goldenrod; ; border-color: goldenrod"  type="button" data-toggle="modal" data-target="#editDistrict"

                                                > <i class = "icon icon-edit"></i></button>
                                            </center>
                                        </td>

                                    </tr>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    @include('template.logoutView')

    @include('template.createuserView')

    @include('template.changepasswordview')
    <div id="delDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="deletecustomer" method="POST">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff">
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-trash icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 14px">Customer  Category</h4>

                        </div>
                    </div>
                    <div class="modal-tabs">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Delete <span id="nameDelete"></span>  From System?</a></li>

                        </ul>
                        <input type="hidden" id="idDelete" name="idDelete"/>

                    </div>
                    <div class="modal-footer">
                        <center><button type="submit" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-check"></i>&nbsp &nbsp Yes &nbsp &nbsp</button>
                            <button type="button" class="btn btn-danger" style="background-color: maroon"><i class="icon icon-close"></i> Cancel</button></center>

                    </div>
                </div>
            </form>
        </div>
    </div>


    <div id="editDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="updatecustomer" method="post" data-parsley-validate="" id="editform">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-edit icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 12px">Edit Customer Details</h4>

                        </div>
                    </div>
                    <div class="modal-tabs">

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="display3">

                                <div class="form-group">

                                    <div class="row">
                                        <input type="hidden" id="idEdit" name="idEdit"/>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Customer Category</label>
                                            <select id="customercategory_idEdit" class="form-control" name="customercategory_idEdit" style="font-size: 11px">
                                                <option value="" >Select Role</option>


                                            </select>

                                        </div>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Name</label>
                                            <input id="nameEdit" name="nameEdit"  class="form-control" type="text" style="font-size: 11px" required=""></div>



                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Phone Number</label>
                                            <input id="phonenumberEdit" class="form-control" type="text" name="phonenumberEdit" style="font-size: 11px" required>

                                        </div>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Address</label>
                                            <input id="addressEdit" class="form-control" type="text" name="addressEdit" style="font-size: 11px" required>
                                        </div>

                                    </div></div>





                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="submit" id="savee" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="newDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="savebill" method="POST" data-parsley-validate="" id="storeform">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-money icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 12px">New Bill</h4>

                        </div>
                    </div>

                    <div class="modal-tabs">

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="display2">
                                <input type="hidden" id="number_of_items" name="number_of_items" value="1" />
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Customer :</label>
                                            <select id="demo-select2-3" class="form-control" name="customer" style="font-size: 11px" required>
                                                <option value="" >Select Customer</option>
                                               @foreach(App\Customer::all() as $c)
                                                    <option value="{{$c->id}}" >{{$c->name}}</option>
                                                   @endforeach

                                            </select>

                                        </div>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Bill number</label>
                                            <input id="billnumber" class="form-control" type="text" name="billnumber" value="{{uniqid().date('d').date('m').date('Y')}}" readonly style="font-size: 11px" required>
                                        </div>
                                    </div>
                                    <hr >
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-hover table-inverse">
                                                <thead>
                                                <tr>
                                                    <th>Product</th>
                                                    <th width="10%">quantity</th>
                                                    <th>Amount (GHC)</th>
                                                    <th>Total (GHC)</th>
                                                    <th> <button type="button" class="btn btn-primary " id="addnew" style="background-color: #217345"><span class="icon icon-plus-circle"></span></button></th>
                                                </tr>
                                                </thead>
                                                <tbody id="data"></tbody>
                                            </table>
                                        </div>
                                      </div>
                                </div>
                             </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" value="Submit"  id="save_data" class="btn btn-primary" style="background-color: #217345">
                    </div>


                </div>
            </form>
        </div>
    </div>





    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/elephant.min.js"></script>
    <script src="assets/js/application.min.js"></script>
    <script src="assets/js/demo.min.js"></script>
    <script src="assets/parsley/js/parsley.min.js"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-83990101-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="assets/js/toastr.min.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {
            var currentItem = 0;
            var datax;

            $.ajax({
                type:"GET",
                url:"{{url('billingselectbox')}}",
                success: function(data) {
                   datax=data;
                }
            });

            $('#addnew').click(function(){
                currentItem =currentItem+1;
                $('#number_of_items').val(currentItem);
                var strToAdd='<tr class="item">' +
                        '<td><div class="form-group"><select name="productname[]" class="form-control"  >'+datax+'</select> </div></td>' +
                        '<td><div class="form-group"><input name="quantity[]" class="form-control" type="number" /> </div> </td> ' +
                        '<td><div class="form-group"> <input name="amount[]" class="form-control"  type="number" /> </div> </td> ' +
                        '<td><div class="form-group"> <input name="total[]" class="form-control"  type="number" /> </div> </td>' +
                        '<td><button type="button" class="btn btn-primary remove" name="remove"  style="background-color: darkred"><span class="icon icon-minus-circle"></span></button> </td>'+
                        ' </tr>';
                $('#data').append(strToAdd);
            });
            $(document).on('click','.remove',function () {
                $(this).closest('tr').remove();
            })



        })

    </script>




</body>
</html>